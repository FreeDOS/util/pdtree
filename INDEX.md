# pdTree

Graphically displays the folder structure of a drive or path. Supports long file names if LFN api present, support for message catalogs (different languages) using cats, and supports both Windows NT/9x and DOS.

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## PDTREE.LSM

<table>
<tr><td>title</td><td>pdTree</td></tr>
<tr><td>version</td><td>1.02-1a</td></tr>
<tr><td>entered&nbsp;date</td><td>2004-05-22</td></tr>
<tr><td>description</td><td>Display the folder structure of a drive or path.</td></tr>
<tr><td>keywords</td><td>tree, FreeDOS, DOS, Win32</td></tr>
<tr><td>author</td><td>Kenneth J. Davis &lt;jeremyd@computer.org&gt;</td></tr>
<tr><td>maintained&nbsp;by</td><td>Kenneth J. Davis &lt;jeremyd@computer.org&gt;</td></tr>
<tr><td>platforms</td><td>DOS (TC30, BC31), Win32 (VC5, BCC55)</td></tr>
<tr><td>copying&nbsp;policy</td><td>Public Domain (optional use of LGPL catgets by Jim Hall)</td></tr>
<tr><td>summary</td><td>Graphically displays the folder structure of a drive or path. Supports long file names if LFN api present, support for message catalogs (different languages) using cats, and supports both Windows NT/9x and DOS.</td></tr>
</table>
